package lv.tsi.javacourses.boundary;
import lv.tsi.javacourses.dto.AcademicPerformanceDTO;
import lv.tsi.javacourses.entity.Academicperformance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class AcdperfSearch implements Serializable {
    @PersistenceContext
    private EntityManager entityManager;
    @Inject
    private CurrentUser currentUser;
    private List<AcademicPerformanceDTO> searchList = new ArrayList<>();
    private String searchWord;

    public void getSearch() {
        String keyword = "%" + getSearchWord().toLowerCase() + "%";
        List<Academicperformance> fullSearch =
                entityManager.createQuery(" select a from Academicperformance a " +
                        " join Student s on a.student.id = s.id " +
                        " join Discipline d on a.discipline.disc_id = d.disc_id where" +
                        " lower(s.stnum )like :keyword " +
                        " or lower(s.fullName)like :keyword " +
                        " or lower(d.name)like :keyword " +
                        " order by a.control, d.name ", Academicperformance.class)
                        .setParameter("keyword", keyword)
                        .getResultList();

        createAcademicPerformanceDto(fullSearch, searchList);
    }

    static void createAcademicPerformanceDto(List<Academicperformance> fullSearch, List<AcademicPerformanceDTO> searchList) {
        fullSearch.forEach(a -> {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            AcademicPerformanceDTO apDTO = new AcademicPerformanceDTO();
            apDTO.setStNum(a.getStudent().getStnum());
            apDTO.setFullName(a.getStudent().getFullName());
            apDTO.setDisciplineName(a.getDiscipline().getName());
            apDTO.setTypeOfControl(a.getControl());
            apDTO.setMark(a.getEvaluation());
            apDTO.setDate(sdf.format(a.getDateofevaluation()));
            searchList.add(apDTO);
        });
    }

    public List<AcademicPerformanceDTO> getSearchList() {
        return searchList;
    }

    public void setSearchList(List<AcademicPerformanceDTO> searchList) {
        this.searchList = searchList;
    }

    public CurrentUser getCurrentUser() {
        return currentUser;
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }
}
