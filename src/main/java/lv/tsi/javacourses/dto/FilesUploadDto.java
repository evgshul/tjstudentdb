package lv.tsi.javacourses.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilesUploadDto {

    private Long id;
    private String studentName;
    private String studentNum;
    private String Discipline;
    private String typeOfControl;
    private String fileName;
    private String fileContent;
    private String fileType;
    private long fileSize;
    private String dateOfUpload;

}
