package lv.tsi.javacourses.boundary;

import lv.tsi.javacourses.control.EmailSender;
import lv.tsi.javacourses.control.UserControl;
import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 */
@ViewScoped
@Named
public class ResetPasswordForm implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordForm.class);
    @Inject
    private EmailSender emailSender;
    @Inject
    private UserControl userControl;
    private Long userId;
    private String email;
    private String confirmationCode;
    private String password1;
    private String password2;
    private boolean confirmationSend;
    private boolean editbutton = false;

    @Transactional
    public void request() {
        ResourceBundle bundle = ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        User u = userControl.findUserByEmail(email, true);
        if (u == null) {
            Util.addError("resetPassword:email", bundle.getString("reset.emailValidation"));
            return;
        }
        String code = emailSender.sendConfirmationCodeResetPassword(email);
        u.setConfirmationCode(code);
        confirmationSend = true;
    }

    @Transactional
    public String confirm() {
        ResourceBundle bundle = ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        User u = userControl.findUserByEmail(email, true);
        if (u == null) {
            Util.addError("resetPassword:email",bundle.getString("reset.emailValidation"));
            return null;
        }
        if (!Objects.equals(password1, password2)) {
            Util.addError("resetPassword:password2", bundle.getString("reset.paswordMatchValid"));
            return null;
        }

        if (Objects.equals(confirmationCode, u.getConfirmationCode())) {
            u.setConfirmationCode(null);
            u.setPassword(userControl.hashPassword(password1));
            return "/sign-in.xhtml?faces-redirect=true";
        } else {
            Util.addError("resetPassword:confirmationCode", bundle.getString("reset.confirmCodeValidation"));
            return null;
        }
    }

    public void edit(){
        editbutton = true;
    }

    @Transactional
    public String submit() {
        userControl.resetPass(userId, password1);
        editbutton = false;
        Util.addConfirmForPrime("Password has successful changed");
       return null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public boolean isConfirmationSend() {
        return confirmationSend;
    }

    public void setConfirmationSend(boolean confirmationSend) {
        this.confirmationSend = confirmationSend;
    }

    public boolean isEditbutton() {
        return editbutton;
    }

    public void setEditbutton(boolean editbutton) {
        this.editbutton = editbutton;
    }
}
