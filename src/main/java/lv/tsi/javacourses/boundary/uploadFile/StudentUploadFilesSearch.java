package lv.tsi.javacourses.boundary.uploadFile;

import lombok.Getter;
import lombok.Setter;
import lv.tsi.javacourses.dto.FilesUploadDto;
import lv.tsi.javacourses.entity.FilesUpload;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Named
public class StudentUploadFilesSearch implements Serializable {
    @PersistenceContext
    private EntityManager manager;
    @Getter
    @Setter
    private List<FilesUploadDto> filesUploadDtoList;
    @Inject
    private StudentUploadFilesForm studentUploadFilesForm;

    public void allStudentUploadFiles() {
        List<FilesUpload> filesUploadList =
                manager.createQuery("select f from FilesUpload f join Student s on f.student.id = s.id " +
                        "join Discipline d on f.discipline.disc_id = d.disc_id " +
                        " order by f.student.fullName ")
                        .getResultList();

        final List<FilesUploadDto> uploadFilesList = new ArrayList<>();
        filesUploadList.forEach(f -> {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            FilesUploadDto filesUploadDtoList = new FilesUploadDto();
            filesUploadDtoList.setId(f.getId());
            filesUploadDtoList.setStudentNum(f.getStudent().getStnum());
            filesUploadDtoList.setStudentName(f.getStudent().getFullName());
            filesUploadDtoList.setFileName(f.getFileName());
            filesUploadDtoList.setDiscipline(f.getDiscipline().getName());
            filesUploadDtoList.setTypeOfControl(f.getControlType());
            filesUploadDtoList.setFileType(f.getContentType());
            filesUploadDtoList.setFileContent(f.getFileContent());
            filesUploadDtoList.setFileSize(f.getFileSize());
            filesUploadDtoList.setDateOfUpload(sdf.format(f.getUploadDate()));
            uploadFilesList.add(filesUploadDtoList);

        });
        studentUploadFilesForm.setDtoList(uploadFilesList);
    }
}
