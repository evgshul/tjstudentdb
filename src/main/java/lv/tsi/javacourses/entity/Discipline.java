package lv.tsi.javacourses.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Entity
@Table(name = "Discipline")
public class Discipline implements Serializable {
    @Id
    @GeneratedValue
    private Long disc_id;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "discipline")
    private Set<Academicperformance> academicperformanceSet;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "discipline")
    private Set<FilesUpload> filesUploads;
    @Column (name = "Discipline_name")
    private String name;

    public Long getId() {
        return disc_id;
    }

    public void setId(Long id) {
        this.disc_id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDisc_id() {
        return disc_id;
    }

    public void setDisc_id(Long disc_id) {
        this.disc_id = disc_id;
    }

//    public Set<Academicperformance> getAcademicperformanceSet() {
//        return academicperformanceSet;
//    }
//
//    public void setAcademicperformanceSet(Set<Academicperformance> academicperformanceSet) {
//        this.academicperformanceSet = academicperformanceSet;
//    }



    @Override
    public String toString() {
        return "Discipline{" +
                "id=" + disc_id +
                ", name='" + name + '\'' +
                '}';
    }
}
