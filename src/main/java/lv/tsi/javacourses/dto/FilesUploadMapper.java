package lv.tsi.javacourses.dto;

import lv.tsi.javacourses.entity.FilesUpload;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface FilesUploadMapper {

    FilesUploadMapper INSTANCE = Mappers.getMapper(FilesUploadMapper.class);

    @Mapping(source = "", target = "")
    FilesUploadDto fileUploadToFileUploadDto(FilesUpload filesUpload);
}
