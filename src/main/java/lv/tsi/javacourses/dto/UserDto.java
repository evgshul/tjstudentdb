package lv.tsi.javacourses.dto;

import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 3/4/2020
 */

public class UserDto {

    private Long id;
    private String stId;
    private String fullName;
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStId() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
