package lv.tsi.javacourses.boundary;


import lv.tsi.javacourses.entity.Student;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Named
public class StudentSearch {
    @PersistenceContext
    private EntityManager em;
    @Inject
    private StudentsSearchForm studentSearchForm;

    public void doSearch() {
        Query q = em.createQuery("SELECT s from Student s where " +
                "UPPER(s.stnum) like :cond " +
                "OR UPPER(s.fullName) like :cond " +
                "OR UPPER(s.email) like :cond " +
                "OR UPPER(s.phone) like :cond ");

        String cond = "%" + studentSearchForm.getCond().toUpperCase() + "%";
        q.setParameter("cond", cond);
        studentSearchForm.setSearchResult(q.getResultList());
    }

    public void setEvuluatuionSt() {
        Query query = em.createQuery("SELECT st FROM Student st where " +
                "UPPER(st.stnum) like :cond " +
                "OR UPPER(st.fullName) like :cond ");
        String cond = "%" + studentSearchForm.getCond().toUpperCase() + "%";
        query.setParameter("cond", cond);
        studentSearchForm.setSearchResult(query.getResultList());
    }
}
