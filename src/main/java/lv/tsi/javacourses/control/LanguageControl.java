package lv.tsi.javacourses.control;


import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 6/4/2020
 */
@Named
@SessionScoped
public class LanguageControl implements Serializable {

    private Locale locale;
    private boolean hideLangEn = true;
    private boolean hideLangLv;
    private boolean hideLangRu;

    public String getLanguage(){

       return getLocale().getLanguage();
    }

    public void setLanguage(String language){
        locale = new Locale(language);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(language));
        hideLang(language);
    }

    public Locale getLocale() {
        if (locale == null)
            locale = FacesContext.getCurrentInstance().getApplication().getDefaultLocale();
        return (locale !=null) ? locale : Locale.getDefault();
    }

    public void hideLang(String language) {
        if (language.equals("en_US")) {
            hideLangEn = true;
            hideLangLv = false;
            hideLangRu = false;
        } else if (language.equals("lv_LV")) {
            hideLangLv = true;
            hideLangEn = false;
            hideLangRu = false;
        } else {
            hideLangRu = true;
            hideLangEn = false;
            hideLangLv = false;
        }
    }

    public boolean isHideLangEn() {
        return hideLangEn;
    }

    public void setHideLangEn(boolean hideLangEn) {
        this.hideLangEn = hideLangEn;
    }

    public boolean isHideLangLv() {
        return hideLangLv;
    }

    public void setHideLangLv(boolean hideLangLv) {
        this.hideLangLv = hideLangLv;
    }

    public boolean isHideLangRu() {
        return hideLangRu;
    }

    public void setHideLangRu(boolean hideLangRu) {
        this.hideLangRu = hideLangRu;
    }
}
