package lv.tsi.javacourses.entity;

public enum TypeOfControl {

    Controlworks_1,
    Controlworks_2,
    Controlworks_3,
    Controlworks,
    Courseworks,
    Exam;
}
