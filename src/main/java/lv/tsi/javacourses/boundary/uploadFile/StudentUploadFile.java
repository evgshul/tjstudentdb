package lv.tsi.javacourses.boundary.uploadFile;

import lombok.Getter;
import lombok.Setter;
import lv.tsi.javacourses.boundary.CurrentUser;
import lv.tsi.javacourses.control.ResourceBundleHelper;
import lv.tsi.javacourses.control.StudentControl;
import lv.tsi.javacourses.control.UploadFileHelper;
import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.dto.FilesUploadMapper;
import lv.tsi.javacourses.entity.Discipline;
import lv.tsi.javacourses.entity.FilesUpload;
import lv.tsi.javacourses.entity.Student;
import lv.tsi.javacourses.entity.TypeOfControl;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static lv.tsi.javacourses.entity.TypeOfControl.Controlworks_1;
import static lv.tsi.javacourses.entity.TypeOfControl.Controlworks_2;
import static lv.tsi.javacourses.entity.TypeOfControl.Controlworks_3;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@RequestScoped
@Named
public class StudentUploadFile implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(StudentUploadFile.class);

    @Getter@Setter
    private UploadedFile file;

    @Inject
    private UploadFileHelper uploadFileHelper;
    @Inject
    private ResourceBundleHelper resourceBundleHelper;
    @PersistenceContext
    EntityManager em;
    @Inject
    private CurrentUser loggedUser;
    @Inject
    private Student student;
    @Inject
    private StudentControl studentControl;
    @Getter@Setter
    private Long discId;
    @Getter@Setter
    private Discipline discipline;
    @Getter@Setter
    private String controlType;
    @Getter@Setter
    private List<Discipline> discList;

    @Transactional
    public String uploadAndSave() throws Exception {

        final String currentUserStudentId = loggedUser.getSignedInUser().getStId();
        student = studentControl.findStudentById(currentUserStudentId.toLowerCase());
        if (student != null) {
            logger.debug("logged user defined: ", student.getFullName());

            FilesUpload filesUpload = new FilesUpload();
            filesUpload.setStudent(student);
            filesUpload.setDiscipline(findDisciplineById(discId));
            filesUpload.setControlType(controlType);
            filesUpload.setFileName(file.getFileName());
            filesUpload.setContentType(file.getContentType());
            filesUpload.setFileSize(file.getSize());
            filesUpload.setFileContent(uploadFileHelper.uploadWithPrime(file));
            filesUpload.setUploadDate(new Date());
            em.persist(filesUpload);

            logger.debug("file successful saved");
            Util.addConfirm("save", resourceBundleHelper.getTranslateBundle("userSpace.uploadFile.success"));
        } else {
            Util.addError("userUploadFiles:upload", "No student, Please contact Admin");
            return null;
        }
        return EMPTY;
    }

    @Transactional
    public Discipline findDisciplineById(Long discId) {
        return em.find(Discipline.class, discId);
    }

    @Transactional
    public List<Discipline> listDiscipline() {
        return discList = em.createQuery("select d from Discipline d order by d.name", Discipline.class).getResultList();
    }

    /**
     * Method represent list of type control for student works upload.
     * @return list types of controls.
     */
    public final List<TypeOfControl> typeOfControlList_1() {
        return Arrays.stream(TypeOfControl.values()).filter(t ->
                !t.equals(Controlworks_1)
                        && !t.equals(Controlworks_2)
                        && !t.equals(Controlworks_3)).collect(Collectors.toList());
    }
}
