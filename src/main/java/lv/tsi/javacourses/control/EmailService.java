package lv.tsi.javacourses.control;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 04-Nov-19
 */

import lv.tsi.javacourses.boundary.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Date;
import java.util.ResourceBundle;


@Stateless
public class EmailService {
    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);
    private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    @Resource(lookup = "java:jboss/mail/gmail")
    private Session session;

    @Inject
    private CurrentUser currentUser;

    public void sendEmail(String email, String name, String text, Part file) {
        ResourceBundle bundle = ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        if (!email.matches(EMAIL_REGEX)) {
            Util.addError("contactus:email", bundle.getString("registration.email.validatorMsg"));
            return;
        }
        Message message = new MimeMessage(session);
        try {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("tatjana@jurkevich.attorney"));
            message.setFrom(new InternetAddress(email));
            message.setSubject("From " + name);
            message.setSentDate(new Date());
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(text, "text/plain:charset=UTF-8");
            messageBodyPart.setContent("Student info: " + "\n" + "Student ID -" + currentUser.getSignedInUser().getStId()
                    + "\n" + "Student FullName -" + currentUser.getSignedInUser().getFullName()
                    + "\n" + "Student email -" + currentUser.getSignedInUser().getEmail() +
                    "\nMessage: " + text, "text/plain;charset=UTF-8");

            Multipart multipart = new MimeMultipart();

            if (file!=null) {
                MimeBodyPart attachpart = new MimeBodyPart();
                DataSource source = new ByteArrayDataSource(file.getInputStream(), "application/x-any");
                attachpart.setDataHandler(new DataHandler(source));
                attachpart.setFileName(file.getSubmittedFileName());
                multipart.addBodyPart(attachpart);
            }
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Transport.send(message);
            logger.info("email sent ");
            Util.addConfirm("contactus:send", bundle.getString("userSpace.sendEmail.confirmMsg"));
        } catch (MessagingException | IOException e) {
            logger.error("Cannot send mail ", e);
        }
    }

}
