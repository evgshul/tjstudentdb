package lv.tsi.javacourses.boundary;


import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.entity.*;


import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@ViewScoped
@Named
public class StudentEvaluation implements Serializable {

    private Long studentId;
    private String fullName;
    private Student student;
    private Long disciplId;
    private Discipline disciplin;
    private List<Discipline> discipline;
    private Date dateofevaluation;
    private String evaluation;
    private String control;
    private Academicperformance academicperformance;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void findDisc() {
        disciplin = em.find(Discipline.class, disciplId);
    }

    @Transactional
    public void findStudent() {
        student = em.find(Student.class, studentId);
    }

    public SelectItem[] listOfMarks() {
        SelectItem[] marks = new SelectItem[Evaluation.values().length];
        int i = 0;
        for (Evaluation e : Evaluation.values()) {
            marks[i++] = new SelectItem(e.getLabel());
        }
        return marks;
    }

    public TypeOfControl[] getTypeOfControl() {
     return TypeOfControl.values();
    }

    @Transactional
    public List<Discipline> listOfDisciplin() {
        return discipline = em.createQuery("select d from Discipline d order by d.name", Discipline.class).getResultList();
    }

    @Transactional
    public String studEvaluations() {
        Query query = em.createQuery("select a from Academicperformance a where" +
                " a.student.id=:studid " +
                " and a.control=:control " +
                " and a.discipline.disc_id=:discid");
        query.setParameter("studid", studentId);
        query.setParameter("control", control);
        query.setParameter("discid", disciplId);
        if (query.getResultList().size() > 0) {
            Util.addError("setevoluation:mark", "Confirmation Error: This records already exist!");
            return null;
        }
        Academicperformance studContr = new Academicperformance();
        studContr.setControl(control);
        studContr.setDateofevaluation(dateofevaluation);
        studContr.setEvaluation(evaluation);
        findDisc();
        studContr.setDiscipline(disciplin);
        findStudent();
        studContr.setStudent(student);
        em.persist(studContr);
        return "/admin-space/searchStudentForEvaluation.xhtml?faces-redirect=true";
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Long getDisciplId() {
        return disciplId;
    }

    public void setDisciplId(Long disciplId) {
        this.disciplId = disciplId;
    }

    public Discipline getDisciplin() {
        return disciplin;
    }

    public void setDisciplin(Discipline disciplin) {
        this.disciplin = disciplin;
    }

    public List<Discipline> getDiscipline() {
        return discipline;
    }

    public void setDiscipline(List<Discipline> discipline) {
        this.discipline = discipline;
    }

    public Date getDateofevaluation() {
        return dateofevaluation;
    }

    public void setDateofevaluation(Date dateofevaluation) {
        this.dateofevaluation = dateofevaluation;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public Academicperformance getAcademicperformance() {
        return academicperformance;
    }

    public void setAcademicperformance(Academicperformance academicperformance) {
        this.academicperformance = academicperformance;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
