package lv.tsi.javacourses.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;


@Entity
@Table(name = "student")
public class Student implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student", orphanRemoval = true)
    private Set<Academicperformance> academicperformanceSet;
    @Column
    private String stnum;
    @Column
    private String fullName;
    @Column
    private String email;
    @Column
    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStnum() {
        return stnum;
    }

    public void setStnum(String stnum) {
        this.stnum = stnum;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Academicperformance> getAcademicperformanceSet() {
        return academicperformanceSet;
    }

    public void setAcademicperformanceSet(Set<Academicperformance> academicperformanceSet) {
        this.academicperformanceSet = academicperformanceSet;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", academicperformanceSet=" + academicperformanceSet +
                ", stnum='" + stnum + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
