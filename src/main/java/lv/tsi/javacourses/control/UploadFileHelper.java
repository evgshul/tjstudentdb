package lv.tsi.javacourses.control;

import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.io.File;

import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 8/11/2020
 */
@Stateless
public class UploadFileHelper {

    private static final Logger logger = LoggerFactory.getLogger(UploadFileHelper.class);

    /**
     *
     * @param file incoming file.
     * @return uploaded content path.
     * @throws Exception
     */
    public String uploadWithPrime(UploadedFile file) throws Exception {
        String usersUploadDir = EMPTY;
        if (file.getSize() > 0) {
            logger.debug("Starting upload file: file name: " + file.getFileName());
            usersUploadDir = prepareUsersUploadDir();
            File uploadFile = new File(usersUploadDir);
            if (!uploadFile.exists()) {
                uploadFile.mkdirs();
            }
            file.write(usersUploadDir + File.separator);
            logger.debug("File uploaded successful: ".concat(" ").concat(file.getFileName()));
        } else {
            logger.error("error upload file: ");
        }
        return usersUploadDir.concat(File.separator).concat(file.getFileName());
    }

    /**
     * Method prepared initial path to save file.
     * @return string of path.
     */
    public String prepareUsersUploadDir() {
        return File.separator.concat("opt").concat(File.separator)
                .concat("UploadedFiles").concat(File.separator).concat("StudentUploadFiles");
    }
}
