package lv.tsi.javacourses.control;

import lv.tsi.javacourses.entity.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
public class StudentControl {
    private static final Logger logger = LoggerFactory.getLogger(StudentControl.class);
    @PersistenceContext
    private EntityManager em;

    public boolean findStudentByStudentId(String studentId) {
            return em.createQuery("select s from Student s where s.stnum =:studentId",Student.class)
                    .setParameter("studentId", studentId)
                    .getResultList()
                    .size() > 0;
    }

    public Student inputStudent(String studentId, String fullName, String email) {
        Student student = new Student();
        student.setStnum(studentId);
        student.setFullName(fullName);
        student.setEmail(email);
        em.persist(student);
        return student;
    }

    public Student findStudentById(String studentId) {
        try {
            return (Student) em.createQuery("select s from Student s where LOWER(s.stnum) like " +
                            ":userStudentId", Student.class)
                    .setParameter("userStudentId", studentId)
                    .getSingleResult();
        } catch (NoResultException e) {
            logger.error(String.format("student (studentId: %s) not found", studentId), e);
            return null;
        }
    }
}
