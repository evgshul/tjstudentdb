package lv.tsi.javacourses.boundary;

import lv.tsi.javacourses.control.EmailSender;
import lv.tsi.javacourses.control.StudentControl;
import lv.tsi.javacourses.control.UserControl;
import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.entity.Student;
import lv.tsi.javacourses.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 */
@ViewScoped
@Named
public class RegistrationForm implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationForm.class);
    private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private static final String FULLNAME_REGEX = "(^[\\p{L}\\s'.-]+$)";
    @PersistenceContext
    private EntityManager em;
    @Inject
    private EmailSender emailSender;
    @Inject
    private UserControl userControl;
    @Inject
    private StudentControl studentControl;
    @Inject
    private User user;
    @Inject
    private Student student;

    private Long userId;
    private String stId;
    private String email;
    private String fullName;
    private String password1;
    private String password2;
    private String confirmationCode;
    private boolean awaitConfirmation = false;
    private boolean editmode = false;

    @Transactional
    public void register() {
        ResourceBundle bundle = ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        if (!Objects.equals(password1, password2)) {
            Util.addError("registration:password2",bundle.getString("registration.repeatPassValidMsg"));
            return;
        }

        if (userControl.emailExists(email)) {
            Util.addError("registration:email", bundle.getString("registration.email.existEmailValidMsg"));
            return;
        }
        final String studentId = stId.toUpperCase().trim();
        if (!userControl.studentIdExists(studentId)) {
            User u = userControl.createUser(studentId, email, fullName, password1);
            if (!studentControl.findStudentByStudentId(studentId)) {
                studentControl.inputStudent(studentId, fullName, email);
            }
            String code = emailSender.sendConfirmationCode(email);
            u.setConfirmationCode(code);
            //System.out.println(" code is " +code);
            awaitConfirmation = true;
        } else {
            logger.error("student Id exist");
            Util.addError("registration:stid", "StudentId exist");
        }
    }


    @Transactional
    public String confirm() {
        User u = userControl.findUserByEmail(email, false);
        if (u != null && Objects.equals(u.getConfirmationCode(), confirmationCode)) {
            u.setConfirmed(true);
            return "/sign-in.xhtml?faces-redirect=true";
        } else {
            ResourceBundle bundle = ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale());
            Util.addError("registration:confirmationCode", bundle.getString("registration.confirmationCode.validMsg"));
            return null;
        }
    }

    public void edit() {
        editmode = true;
    }

    public void findUser() {
        user = em.find(User.class, userId);
    }

    @Transactional
    public String saveEditableUser() {

        User editUser = em.find(User.class, userId);
        if (!fullName.isEmpty()) {
            editUser.setFullName(fullName);
        }
        if (!email.isEmpty()) {
            editUser.setEmail(email);
        }
        em.persist(editUser);
        editmode = false;
        Util.addConfirmForPrime("user's dates successful update");
        findUser();
        return null;
    }
    
    public void enableUser() {
        userControl.enableUser(userId,true);
        Util.addConfirmForPrime("User enabled");
    }

    public String getEmailRegex() {
        return EMAIL_REGEX;
    }

    public String getFullnameRegex() {
        return FULLNAME_REGEX;
    }

    public String getStId() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public boolean isAwaitConfirmation() {
        return awaitConfirmation;
    }

    public void setAwaitConfirmation(boolean awaitConfirmation) {
        this.awaitConfirmation = awaitConfirmation;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public boolean isEditmode() {
        return editmode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
