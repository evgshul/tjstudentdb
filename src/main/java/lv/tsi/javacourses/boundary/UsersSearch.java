package lv.tsi.javacourses.boundary;

import lv.tsi.javacourses.dto.UserDto;
import lv.tsi.javacourses.entity.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 3/4/2020
 */
@RequestScoped
@Named
public class UsersSearch implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private String searchWord;
    private List<UserDto> user = new ArrayList<>();

    public void userSearch() {
        String cond = "%" + searchWord.toUpperCase() + "%";
        List<User> resultList = em.createQuery("select u from User u where " +
                "UPPER(u.stId) like :searchWord " +
                "or upper(u.fullName) like :searchWord " +
                "or upper(u.email) like :searchWord ", User.class)
                .setParameter("searchWord", cond)
                .getResultList();

        resultList.forEach(u -> {
            UserDto d = new UserDto();
            d.setId(u.getId());
            d.setStId(u.getStId());
            d.setFullName(u.getFullName());
            d.setEmail(u.getEmail());
            user.add(d);
        });
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    public List<UserDto> getUser() {
        return user;
    }

    public void setUser(List<UserDto> user) {
        this.user = user;
    }
}