package lv.tsi.javacourses.boundary.uploadFile;

import lombok.Getter;
import lombok.Setter;
import lv.tsi.javacourses.dto.FilesUploadDto;
import lv.tsi.javacourses.entity.Discipline;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@Named
public class StudentUploadFilesForm implements Serializable {

    @PersistenceContext
    private EntityManager manager;
    @Getter
    @Setter
    private List<FilesUploadDto> dtoList;
    @Getter
    @Setter
    private String searchCond;
    @Getter
    @Setter
    private Long discSearchId;

    public List<Discipline> allDiscipline() {
        return manager.createQuery("select d from Discipline d order by d.name",
                Discipline.class).getResultList();
    }

    public List<String> disciplineName() {
        List<String> list = new ArrayList<>();
        allDiscipline().forEach(d -> list.add(d.getName()));
        return list;
    }
}
