package lv.tsi.javacourses.control;

import lv.tsi.javacourses.entity.Role;
import lv.tsi.javacourses.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 */
@Stateless
public class UserControl {
    private static final Logger logger = LoggerFactory.getLogger(UserControl.class);
    @PersistenceContext
    private EntityManager em;

    public User findUserByEmail(String email, boolean confirmed) {
        try {
            return em.createQuery("select u from User u where u.email = :email AND u.confirmed = :confirmed", User.class)
                    .setParameter("email", email)
                    .setParameter("confirmed", confirmed)
                    .getSingleResult();
        } catch (NoResultException e) {
            logger.error(String.format("user (email: %s; confirmed: %s) not found", email, confirmed), e);
            return null;
        }
    }


    public Role findRole(String name) {
        return em.createQuery("select r from Role r where r.name = :name", Role.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public String hashPassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            return Base64.getMimeEncoder(76, new byte[]{'\n'}).encodeToString(hash);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            logger.error("This never can happen!", e);
            throw new IllegalStateException(e);
        }
    }

    public boolean emailExists(String email) {
        return em.createQuery("select u from User u where u.email = :email", User.class)
                .setParameter("email", email)
                .getResultList()
                .size() > 0;
    }
    public boolean studentIdExists(String studentId) {
        return em.createQuery("select u from User u where u.stId = :studentId", User.class)
                .setParameter("studentId",studentId)
                .getResultList()
                .size() > 0;
    }

    public User createUser(String stId, String email, String fullName, String password) {
        Role r = findRole("user");
        User u = new User();
        u.setStId(stId);
        u.setEmail(email);
        u.setFullName(fullName);
        u.setPassword(hashPassword(password));
        u.setConfirmed(false);
        u.setRoles(Collections.singleton(r));
        em.persist(u);
        return u;
    }

    public void resetPass(Long id, String password) {
        User existUser = em.find(User.class, id);
        existUser.setPassword(hashPassword(password));
        em.persist(existUser);
    }
    public void enableUser(Long id, boolean confirmed) {
        User existUser = em.find(User.class, id);
        existUser.setConfirmed(confirmed);
        em.persist(existUser);
    }
}
