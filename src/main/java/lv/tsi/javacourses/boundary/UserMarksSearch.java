package lv.tsi.javacourses.boundary;

import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.dto.AcademicPerformanceDTO;
import lv.tsi.javacourses.entity.Academicperformance;
import lv.tsi.javacourses.entity.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 7/31/2020
 */
@Named
@ViewScoped
public class UserMarksSearch implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(AcdperfSearch.class);
    @Inject
    private CurrentUser currentUser;
    @PersistenceContext
    private EntityManager entityManager;
    private List<AcademicPerformanceDTO> studentPerformance = new ArrayList<>();

    //Check if the User is registred in Student DB
    public String isStudentEqualSignInUser() {
        String stId;
        try {
            stId = currentUser.getSignedInUser().getStId().toLowerCase();
        } catch (NullPointerException e) {
            logger.error("No Such User", e);
            return null;
        }
        if (!isStIdExist(stId)) {
            ResourceBundle bundle = ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale());
            Util.addError("studentArea:btnMarks", bundle.getString("userSpace.messageWarning.noStudent"));
            return null;
        }
        return "/user-space/perfsearch.xhtml?faces-redirect=true";
    }

    public void loggedUserMarksSearch() {
        String signInUser = currentUser.getSignedInUser().getStId().toLowerCase();
        List<Academicperformance> stReport = entityManager.createQuery("select a " +
                " from Academicperformance a " +
                " join Student s on s.id = a.student.id " +
                " join Discipline d on d.disc_id = a.discipline.disc_id " +
                " where lower(s.stnum) like :keyword " +
                " order by d.name", Academicperformance.class)
                .setParameter("keyword", signInUser)
                .getResultList();

        AcdperfSearch.createAcademicPerformanceDto(stReport, studentPerformance);
    }

    public boolean isStIdExist(String stId) {
        return entityManager.createQuery("select s from Student s where lower (s.stnum ) = :stId", Student.class)
                .setParameter("stId", stId)
                .getResultList()
                .size() > 0;
    }

    //Getters and Setters
    public List<AcademicPerformanceDTO> getStudentPerformance() {
        return studentPerformance;
    }

    public void setStudentPerformance(List<AcademicPerformanceDTO> studentPerformance) {
        this.studentPerformance = studentPerformance;
    }
}
