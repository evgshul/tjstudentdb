package lv.tsi.javacourses.dto;

import lv.tsi.javacourses.entity.Discipline;
import lv.tsi.javacourses.entity.Student;

import java.util.Date;

/**
 * @Author Evgeny Shulgin, evgshul@gmail.com, 3/14/2020
 */
public class AcademicPerformanceDTO {
    private String stNum;
    private String fullName;
    private String disciplineName;
    private String typeOfControl;
    private String mark;
    private String date;


    public String getStNum() {
        return stNum;
    }

    public void setStNum(String stNum) {
        this.stNum = stNum;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName;
    }

    public String getTypeOfControl() {
        return typeOfControl;
    }

    public void setTypeOfControl(String typeOfControl) {
        this.typeOfControl = typeOfControl;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
