package lv.tsi.javacourses.boundary.uploadFile;
import lv.tsi.javacourses.entity.FilesUpload;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

@ViewScoped
@Named
public class DownLoadFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @PersistenceContext
    private EntityManager em;


    public void downloadFile() throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext externalContext = fc.getExternalContext();
        final Map<String,String> params = externalContext.getRequestParameterMap();
        final String strId= params.get("id");
        Long id = Long.parseLong(strId);

        FilesUpload filesUpload = em.find(FilesUpload.class,id);
        final File file = new File(filesUpload.getFileContent());
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        response.reset();
        response.setHeader("Content-Type","application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=" + filesUpload.getFileName());
        response.setContentLength((int) filesUpload.getFileSize());
        OutputStream outputStream = response.getOutputStream();
        InputStream is = new FileInputStream(file);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = is.read(buffer)) > 0) {
            outputStream.write(buffer, 0 ,bytesRead);
        }
        outputStream.flush();
        is.close();
        outputStream.close();
        fc.responseComplete();
    }
}
