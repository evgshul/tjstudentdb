package lv.tsi.javacourses.boundary;


import lombok.Getter;
import lombok.Setter;
import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.entity.Academicperformance;
import lv.tsi.javacourses.entity.Discipline;
import lv.tsi.javacourses.entity.Student;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@ViewScoped
@Named
public class UpdateExam implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private String newMark;
    private Date dateofexam;
    private Academicperformance academicperformance;
    private Long mark_Id;
    private String searchword;
    private List<Object[]> resultexist;
    private Long stId;
    private Student student;
    @Getter
    @Setter
    private Long disciplineId;
    @Inject
    @Getter@Setter
    private Discipline discipline;


    @Transactional
    public void findStudent() {
        student = em.find(Student.class, stId);
    }


    public void searchExistExam() {
        Query existExam = em.createQuery("Select a.mark_id, s.stnum, s.fullName, d.name, d.disc_id, a.control, a.evaluation, s.id " +
                "from Academicperformance a " +
                "join Student s on a.student.id = s.id " +
                "join Discipline d on a.discipline.disc_id = d.disc_id" +
                " where (a.control like 'Exam')" +
                " and (UPPER(s.stnum)like :keyword or" +
                " UPPER(s.fullName)like :keyword)");

        String keyword = "%" + searchword.toUpperCase() + "%";
        existExam.setParameter("keyword", keyword);

        setResultexist(existExam.getResultList());

    }

    @Transactional
    public String updateExam() {

        Academicperformance changeMark = em.find(Academicperformance.class, mark_Id);
        changeMark.setDateofevaluation(dateofexam);
        changeMark.setEvaluation(newMark);
        em.persist(changeMark);
        Util.addConfirm("updateexam:mark", "You successfull update exam mark");
        return null;
    }

    public void findDiscipline() {
        discipline = em.find(Discipline.class, disciplineId);
    }

    public String getNewMark() {
        return newMark;
    }

    public void setNewMark(String newMark) {
        this.newMark = newMark;
    }

    public Date getDateofexam() {
        return dateofexam;
    }

    public void setDateofexam(Date dateofexam) {
        this.dateofexam = dateofexam;
    }

    public Academicperformance getAcademicperformance() {
        return academicperformance;
    }

    public void setAcademicperformance(Academicperformance academicperformance) {
        this.academicperformance = academicperformance;
    }

    public Long getMark_Id() {
        return mark_Id;
    }

    public void setMark_Id(Long mark_Id) {
        this.mark_Id = mark_Id;
    }

    public List<Object[]> getResultexist() {
        return resultexist;
    }

    public void setResultexist(List<Object[]> resultexist) {
        this.resultexist = resultexist;
    }

    public String getSearchword() {
        return searchword;
    }

    public void setSearchword(String searchword) {
        this.searchword = searchword;
    }

    public Long getStId() {
        return stId;
    }

    public void setStId(Long stId) {
        this.stId = stId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
