package lv.tsi.javacourses.control;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

@Stateless
public class ResourceBundleHelper {

    public String getTranslateBundle(String key){
        return ResourceBundle.getBundle("AplicationResources", FacesContext.getCurrentInstance().getViewRoot().getLocale()).getString(key);
    }
}
