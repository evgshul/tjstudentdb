package lv.tsi.javacourses.boundary;


import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.entity.Student;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;


@Stateless
@Named
public class DeleteStudent {
    @PersistenceContext
    private EntityManager em;

    private Long studentId;
    private Student student;
    private String keyword;
    @Inject
    private StudentsSearchForm studentsSearchForm;


    public void searchStudent() {
        Query st = em.createQuery("select s from Student s " +
                " where UPPER(s.stnum) like :key ");

        String key = getKeyword().toUpperCase();
        st.setParameter("key", key);
        studentsSearchForm.setSearchResult(st.getResultList());


    }

    @Transactional
    public String removeStudent() {
        Student exstudent = em.find(Student.class, studentId);
        em.remove(exstudent);

        FacesContext.getCurrentInstance()
                .addMessage("studremove:message", new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "All The student data was permanently removed from database!!!"));

        return null;
    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }


    public StudentsSearchForm getStudentsSearchForm() {
        return studentsSearchForm;
    }

    public void setStudentsSearchForm(StudentsSearchForm studentsSearchForm) {
        this.studentsSearchForm = studentsSearchForm;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
