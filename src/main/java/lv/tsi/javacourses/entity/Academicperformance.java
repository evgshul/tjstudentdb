package lv.tsi.javacourses.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "Academic_Performance")
public class Academicperformance implements Serializable {

    @Id
    @GeneratedValue
    private Long mark_id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "disc_id")
    private Discipline discipline;
    @Column(nullable = false)
    private String control;
    @Column(nullable = false)
    private String evaluation;
    @Column
    @Temporal(TemporalType.DATE)
    private Date dateofevaluation;


    public Long getMark_id() {
        return mark_id;
    }

    public void setMark_id(Long mark_id) {
        this.mark_id = mark_id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Discipline getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public Date getDateofevaluation() {
        return dateofevaluation;
    }

    public void setDateofevaluation(Date dateofevaluation) {
        this.dateofevaluation = dateofevaluation;
    }

    @Override
    public String toString() {
        return "Academicperformance{" +
                "mark_id=" + mark_id +
                ", student=" + student +
                ", discipline=" + discipline +
                ", control='" + control + '\'' +
                ", evaluation='" + evaluation + '\'' +
                ", dateofevaluation=" + dateofevaluation +
                '}';
    }
}
