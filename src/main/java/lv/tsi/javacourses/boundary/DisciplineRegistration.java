package lv.tsi.javacourses.boundary;


import lv.tsi.javacourses.control.Util;
import lv.tsi.javacourses.entity.Discipline;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.Serializable;

@RequestScoped
@Named
public class DisciplineRegistration implements Serializable {

    @PersistenceContext
    private EntityManager entityManager;

    private String disciplinename;
    private Discipline discipline;

    @Transactional
    public String regDiscipline() {
        Query checkdiscipline = entityManager.createQuery("Select d From " +
                "Discipline d where d.name = :disciplinename");
        checkdiscipline.setParameter("disciplinename", disciplinename);
        if (checkdiscipline.getResultList().size() > 0) {
            Util.addError("discipline:name", "The Discipline is exist");
            return null;
        }

        Discipline discipline = new Discipline();
        discipline.setName(disciplinename);
        entityManager.persist(discipline);
        Util.addConfirm("discipline:name", "Congratulations you add new Discipline");

        clear();
        return null;

    }

    private void clear() {
        setDisciplinename(null);
    }

    public String getDisciplinename() {
        return disciplinename;
    }

    public void setDisciplinename(String disciplinename) {
        this.disciplinename = disciplinename;
    }

    public Discipline getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }
}
