package lv.tsi.javacourses.control;

import lv.tsi.javacourses.entity.TypeOfControl;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;


public class Util {

    public static void addError(String fieldId, String message) {
        FacesContext.getCurrentInstance()
                .addMessage(fieldId,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    public static void addConfirm(String numberId, String msg) {
        FacesContext.getCurrentInstance()
                .addMessage(numberId, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null));
    }

    public static void addConfirmForPrime(String msg) {
        FacesContext.getCurrentInstance()
                .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", msg));
    }

    public static void errorMessagePrime(String idClient, String msg){
        FacesContext.getCurrentInstance().addMessage(idClient, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", msg));
    }

    public static void infoMessage(String Id, String msg) {
        FacesContext.getCurrentInstance()
                .addMessage(Id, new FacesMessage(FacesMessage.SEVERITY_INFO, msg,null));
    }
}
